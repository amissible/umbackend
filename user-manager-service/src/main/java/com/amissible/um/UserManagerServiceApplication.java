package com.amissible.um;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.ProviderManager;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

@SpringBootApplication
@EnableSwagger2
public class UserManagerServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserManagerServiceApplication.class, args);
	}

	@Bean
	public JdbcTemplate getJdbcTemplate(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://amissible.mynetgear.com:3306/DEV?serverTimezone=EST");
		dataSource.setUsername("osingh");
		dataSource.setPassword("Temp@123");
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		return jdbcTemplate;
	}

}
