package com.amissible.um.config;

import java.util.Arrays;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class CorsConfig {
    private static final Logger logger = LoggerFactory.getLogger(CorsConfig.class);

	@Bean
	public CorsFilter corsFilter() {
		logger.info("CORS entered");
		  final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		    final CorsConfiguration config = new CorsConfiguration();
		    config.setAllowedOrigins(Collections.singletonList("http://localhost:4200")); // Provide list of origins if you want multiple origins
		    config.setAllowedHeaders(Arrays.asList("Origin", "Content-Type", "Accept"));
		    config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"));
		    config.setAllowCredentials(true);
		    source.registerCorsConfiguration("/**", config);
		    return new CorsFilter(source);
	}

}
