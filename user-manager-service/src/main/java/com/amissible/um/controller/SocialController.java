package com.amissible.um.controller;

import com.amissible.um.dao.UserDAO;
import com.amissible.um.model.User;
import com.amissible.um.social.FBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@CrossOrigin
@RestController
public class SocialController {

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @Autowired
    private UsersConnectionRepository connectionRepository;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private FBService fbService;

    // User login with social networking,
    // but does not allow the app to view basic information
    // application will redirect to page / signin.
    @RequestMapping(value = { "/signin" }, method = RequestMethod.GET)
    public String signInPage(WebRequest request, Model model) {
        /*System.out.println("START OF MainController.signInPage()");
        ProviderSignInUtils providerSignInUtils //
                = new ProviderSignInUtils(connectionFactoryLocator, connectionRepository);

        // Retrieve social networking information.
        Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
        //
        AppUserForm myForm = null;
        //
        if (connection != null) {
            if (connection.getKey().getProviderId().equalsIgnoreCase("google")) {
                myForm = new AppUserForm(connection);
            } else if (connection.getKey().getProviderId().equalsIgnoreCase("facebook")) {
                myForm = new AppUserForm(connection,fbService.getProfile("me", connection.createData().getAccessToken()));
            }
            else
                myForm = new AppUserForm(connection);
        } else {
            myForm = new AppUserForm();
        }
        System.out.println("DATA = "+myForm.toString());
        model.addAttribute("myForm", myForm);
        System.out.println("END OF MainController.signInPage()");*/
        return "signInPage";
    }
    /*@RequestMapping(value = { "/signin" }, method = RequestMethod.GET)
    public String signInPage(Model model) {
        return "redirect:/login";
    }*/

    @RequestMapping(value = { "/signup" }, method = RequestMethod.GET)
    public User signupPage(WebRequest request, Model model) {
        User user = null;
        ProviderSignInUtils providerSignInUtils = new ProviderSignInUtils(connectionFactoryLocator, connectionRepository);
        Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
        if (connection != null) {
            UserProfile socialUserProfile;
            if (connection.getKey().getProviderId().equalsIgnoreCase("facebook")) {
                socialUserProfile = fbService.getProfile("me", connection.createData().getAccessToken());
            }
            else
                socialUserProfile = connection.fetchUserProfile();
            String fn = socialUserProfile.getFirstName();
            String ln = socialUserProfile.getLastName();
            String email = socialUserProfile.getEmail();
            String username = socialUserProfile.getUsername();
            if(email != null)
            user = userDAO.getUserByEmail(email);
            if(user == null && username != null)
                user = userDAO.getUserByUsername(username);
            if(user == null) {
                System.out.println("Redirect to Register");
            }
            else{
                System.out.println("Redirect to Load User");
            }
        } else {
            //Redirect to error page
        }
        user.hideSensitiveData(); // Will be deleted
        return user; //Return type will be void
    }
}
