package com.amissible.um.controller;

import com.amissible.um.exception.AppException;
import com.amissible.um.dao.UserDAO;
import com.amissible.um.model.*;
import com.amissible.um.security.jwt.JWTFilter;
import com.amissible.um.security.jwt.TokenProvider;
import com.amissible.um.security.rest.AuthenticationRestController;
import com.amissible.um.security.rest.dto.LoginDto;
import com.amissible.um.service.UMService;
import com.amissible.um.util.TOTPManager;
import com.amissible.um.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping("/um")
public class UMController {

    @Autowired
    UserDAO userDAO;

    @Autowired
    UMService umService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public UMController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }

    @RequestMapping("/verifyLoginData")
    public User verifyLoginData(@RequestParam String username,@RequestParam String password){
        return umService.verifyLoginData(username, password);
    }

    @RequestMapping("/sendOTP")
    public void sendOTP(@RequestParam String email){
        umService.sendOTP(email);
    }

    @RequestMapping("/verifyOTP")
    public Boolean verifyOTP(@RequestParam String email,@RequestParam String otp){
        return umService.verifyOTP(email,otp);
    }

    @RequestMapping("/recover/username")
    public void recoverUsername(@RequestParam String email){
        umService.recoverCredentials(email,"Username");
    }

    @RequestMapping("/recover/password")
    public void recoverPassword(@RequestParam String email){
        umService.recoverCredentials(email,"Password");
    }

    @RequestMapping("/register/addAuthority")
    public void addAuthority(@RequestParam String role){
        umService.addAuthority(role);
    }

    @RequestMapping("/register/removeAuthority")
    public void removeAuthority(@RequestParam String role){
        umService.removeAuthority(role);
    }

    @RequestMapping("/register/getAllAuthorities")
    public List<Authority> getAllAuthorities(){ return umService.getAllAuthorities(); }

    @RequestMapping("/register/assignAuthority")
    public void assignAuthority(@RequestParam String email,@RequestParam String role){
        umService.assignAuthority(email,role);
    }

    @RequestMapping("/register/revokeAllAuthoritiesFromUser")
    public void revokeAllAuthoritiesFromUser(@RequestParam String email){
        umService.revokeAllAuthoritiesFromUser(email);
    }

    @RequestMapping("/register/revokeAuthorityFromUser")
    public void revokeAuthorityFromUser(@RequestParam String email,@RequestParam String role){
        umService.revokeAuthorityFromUser(email,role);
    }

    @RequestMapping("/register/getUserAuthorities")
    public Set<Authority> getUserAuthorities(@RequestParam String email){ return umService.getUserAuthorities(email); }

    @RequestMapping("/register/user")
    public void registerUser(@RequestParam String firstName,@RequestParam String lastName,@RequestParam String email,
                             @RequestParam String username,@RequestParam String password,@RequestParam String roles[]){
        umService.registerUser(firstName,lastName,email,username,passwordEncoder.encode(password),roles);
    }

    //TWO FACTOR AUTHENTICATION

    @RequestMapping(value = "/2FA/enable")
    public @ResponseBody ResponseEntity enable2FA(@RequestParam String email){
        return umService.enable2FA(email);
    }

    @RequestMapping(value = "/2FA/disable")
    public void disable2FA(@RequestParam String email){
        umService.disable2FA(email);
    }

    @RequestMapping(value = "/2FA/verifyTOTP")
    public @ResponseBody Boolean verifyTOTP(@RequestParam String email,@RequestParam String code) {
        return umService.verifyTOTP(email, code);
    }

    // LOADING USER

    @RequestMapping(value = "/loadUser")
    public @ResponseBody ResponseEntity<JWTToken> loadUser(@RequestParam String username,@RequestParam String password,@RequestParam Boolean rememberMe) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,password);
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }
}
