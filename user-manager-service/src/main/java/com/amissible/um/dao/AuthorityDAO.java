package com.amissible.um.dao;

import com.amissible.um.exception.AppException;
import com.amissible.um.model.Authority;
import com.amissible.um.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorityDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private String tableName = "AM_UM_AUTHORITY";

    public List<Authority> getAllAuthorities(){
        List<Authority> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM "+tableName+"", new Object[] { },
                (rs, rowNum) -> new Authority(rs.getString("name"))
        ).forEach(record -> list.add(record));
        return list;
    }

    public Boolean isPresent(Authority authority){
        List<Authority> authorityList = getAllAuthorities();
        for(Authority auth : authorityList){
            if(auth.getName().equals(authority.getName()))
                return true;
        }
        return false;
    }

    public void addAuthority(Authority authority){
        if(isPresent(authority))
            throw new AppException("AUTHORITY_ALREADY_PRESENT", HttpStatus.ALREADY_REPORTED);
        jdbcTemplate.update("INSERT INTO "+tableName+"(name) VALUES(?)",authority.getName());
    }

    public void removeAuthority(Authority authority){
        jdbcTemplate.update("DELETE FROM "+tableName+" WHERE name = ?",authority.getName());
    }

    public void removeAllAuthorities(){
        jdbcTemplate.update("DELETE FROM "+tableName+" WHERE 1");
    }
}
