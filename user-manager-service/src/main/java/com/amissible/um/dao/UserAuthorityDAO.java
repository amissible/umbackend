package com.amissible.um.dao;

import com.amissible.um.exception.AppException;
import com.amissible.um.model.Authority;
import com.amissible.um.model.UserAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserAuthorityDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    AuthorityDAO authorityDAO;

    private String tableName = "AM_UM_USER_AUTHORITY";

    public List<UserAuthority> getAllUserAuthorities(){
        List<UserAuthority> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM "+tableName+"", new Object[] { },
                (rs, rowNum) -> new UserAuthority(rs.getLong("userId"),rs.getString("authorityName"))
        ).forEach(record -> list.add(record));
        return list;
    }
    public Set<Authority> getUserAuthorities(Long userId){
        Set<Authority> list = new HashSet<>();
        jdbcTemplate.query(
                "SELECT * FROM "+tableName+" WHERE userId = ?", new Object[] { userId },
                (rs, rowNum) -> new Authority(rs.getString("authorityName"))
        ).forEach(record -> list.add(record));
        return list;
    }

    public Boolean isPresent(UserAuthority userAuthority){
        Set<Authority> authorityList = getUserAuthorities(userAuthority.getUserId());
        for(Authority auth : authorityList){
            if(auth.getName().equals(userAuthority.getAuthorityName()))
                return true;
        }
        return false;
    }

    public void assignAuthority(Long userId,Authority authority){
        if(!authorityDAO.isPresent(authority))
            throw new AppException("AUTHORITY_NOT_FOUND", HttpStatus.NOT_FOUND);
        if(isPresent(new UserAuthority(userId,authority.getName())))
            throw new AppException("USER_ALREADY_HAS_AUTHORITY", HttpStatus.ALREADY_REPORTED);
        jdbcTemplate.update("INSERT INTO "+tableName+"(userId,authorityName) VALUES(?,?)",userId,authority.getName());
    }

    public void revokeAuthorityFromAllUsers(Authority authority){
        jdbcTemplate.update("DELETE FROM "+tableName+" WHERE authorityName = ?",authority.getName());
    }

    public void revokeAllAuthoritiesFromUser(Long userId){
        jdbcTemplate.update("DELETE FROM "+tableName+" WHERE userId = ?",userId);
    }

    public void revokeAuthorityFromUser(Long userId,Authority authority){
        jdbcTemplate.update("DELETE FROM "+tableName+" WHERE userId = ? AND authorityName = ?",userId,authority.getName());
    }

    public void revokeAllAuthoritiesFromAllUsers(){
        jdbcTemplate.update("DELETE FROM "+tableName+" WHERE 1");
    }

}
