package com.amissible.um.dao;

import com.amissible.um.exception.AppException;
import com.amissible.um.model.Status;
import com.amissible.um.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class UserDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    UserAuthorityDAO userAuthorityDAO;

    private String tableName = "AM_UM_USER";

    public List<User> getAllUsers(){
        List<User> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM "+tableName+"", new Object[] { },
                (rs, rowNum) -> new User(rs.getLong("id"),rs.getString("firstName"),
                        rs.getString("lastName"),rs.getString("email"),
                        rs.getString("username"),rs.getString("password"),
                        rs.getString("status"),rs.getString("registeredOn"),
                        rs.getString("secretKey"),rs.getString("otp"),
                        rs.getString("otpTimeStamp")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public User getUserById(Long id){
        List<User> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM "+tableName+" WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new User(rs.getLong("id"),rs.getString("firstName"),
                        rs.getString("lastName"),rs.getString("email"),
                        rs.getString("username"),rs.getString("password"),
                        rs.getString("status"),rs.getString("registeredOn"),
                        rs.getString("secretKey"),rs.getString("otp"),
                        rs.getString("otpTimeStamp")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public User getUserByUsername(String username){
        List<User> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM "+tableName+" WHERE username = ?", new Object[] { username },
                (rs, rowNum) -> new User(rs.getLong("id"),rs.getString("firstName"),
                        rs.getString("lastName"),rs.getString("email"),
                        rs.getString("username"),rs.getString("password"),
                        rs.getString("status"),rs.getString("registeredOn"),
                        rs.getString("secretKey"),rs.getString("otp"),
                        rs.getString("otpTimeStamp")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public User getUserByEmail(String email){
        List<User> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM "+tableName+" WHERE email = ?", new Object[] { email },
                (rs, rowNum) -> new User(rs.getLong("id"),rs.getString("firstName"),
                        rs.getString("lastName"),rs.getString("email"),
                        rs.getString("username"),rs.getString("password"),
                        rs.getString("status"),rs.getString("registeredOn"),
                        rs.getString("secretKey"),rs.getString("otp"),
                        rs.getString("otpTimeStamp")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Long registerUser(String firstName,String lastName,String email,String username,String password){
        User user = getUserByEmail(email);
        if(user != null)
            throw new AppException("EMAIL_ALREADY_REGISTERED", HttpStatus.ALREADY_REPORTED);
        user = getUserByUsername(username);
        if(user != null)
            throw new AppException("USERNAME_NOT_AVAILABLE", HttpStatus.NOT_ACCEPTABLE);
        jdbcTemplate.update("INSERT INTO "+tableName+"(firstName,lastName,email,username,password,status,secretKey,otp) VALUES(?,?,?,?,?,?,?,?)",firstName,lastName,email,username,password, Status.UNVERIFIED.toString(),"","");
        return getUserByEmail(email).getId();
    }

    public Boolean secretKeyPresent(String secretKey){
        List<Integer> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT COUNT(id) AS SKcount FROM "+tableName+" WHERE secretKey = ?", new Object[] { secretKey },
                (rs, rowNum) -> list.add(rs.getInt("SKcount"))
        );
        return list.get(0) != 0;
    }

    public void updateSecretKey(String email,String secretKey){
        jdbcTemplate.update("UPDATE "+tableName+" SET secretKey = ? WHERE email = ?",secretKey,email);
    }

    public void updateOTP(String email,String otp,String otpTimeStamp){
        jdbcTemplate.update("UPDATE "+tableName+" SET otp = ?,otpTimeStamp = ? WHERE email = ?",otp,otpTimeStamp,email);
    }

    public User getUserByEmailWithAuthority(String email){
        User user = getUserByEmail(email);
        if(user != null)
            user.setAuthorities(userAuthorityDAO.getUserAuthorities(user.getId()));

        return user;
    }

    public User getUserByUsernameWithAuthority(String username){
        User user = getUserByUsername(username);
        if(user != null)
            user.setAuthorities(userAuthorityDAO.getUserAuthorities(user.getId()));
        return user;
    }
}
