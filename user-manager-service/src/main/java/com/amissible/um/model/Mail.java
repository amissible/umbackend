package com.amissible.um.model;

import java.util.Arrays;
import java.util.Map;

public class Mail {

    private String from;
    private String to;
    private String cc;
    private String bcc;
    private String subject;
    private String template;
    private String attachments[];
    Map model;

    public Mail() {
    }

    public Mail(String from, String to, String subject, String template) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.template = template;
    }

    public Mail(String from, String to, String cc, String bcc, String subject, String template) {
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.template = template;
    }

    public Mail(String from, String to, String subject, String template, String[] attachments) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.template = template;
        this.attachments = attachments;
    }

    public Mail(String from, String to, String cc, String bcc, String subject, String template, String[] attachments) {
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.template = template;
        this.attachments = attachments;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String[] getAttachments() {
        return attachments;
    }

    public void setAttachments(String[] attachments) {
        this.attachments = attachments;
    }

    public Map getModel() {
        return model;
    }

    public void setModel(Map model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", cc='" + cc + '\'' +
                ", bcc='" + bcc + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + template + '\'' +
                ", attachments=" + Arrays.toString(attachments) +
                ", model=" + model +
                '}';
    }
}
