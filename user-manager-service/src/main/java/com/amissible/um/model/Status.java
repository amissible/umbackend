package com.amissible.um.model;

public enum Status {
    ACTIVE,
    INACTIVE,
    BLOCKED,
    UNVERIFIED
}
