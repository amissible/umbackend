package com.amissible.um.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private String status; //ACTIVE,INACTIVE,BLOCKED
    private String registeredOn;
    private Boolean is2FAEnabled;
    private String secretKey;
    private String otp;
    private String otpTimeStamp;
    private Set<Authority> authorities = new HashSet<>();

    public User() {
    }

    public User(Long id, String firstName, String lastName, String email, String username, String password, String status, String registeredOn, String secretKey, String otp, String otpTimeStamp, Set<Authority> authorities) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.status = status;
        this.registeredOn = registeredOn;
        this.secretKey = secretKey;
        this.otp = otp;
        this.otpTimeStamp = otpTimeStamp;
        this.authorities = authorities;
    }

    public User(Long id, String firstName, String lastName, String email, String username, String password, String status, String registeredOn, String secretKey, String otp, String otpTimeStamp) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.status = status;
        this.registeredOn = registeredOn;
        this.secretKey = secretKey;
        this.otp = otp;
        this.otpTimeStamp = otpTimeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegisteredOn() {
        return registeredOn;
    }

    public void setRegisteredOn(String registeredOn) {
        this.registeredOn = registeredOn;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpTimeStamp() {
        return otpTimeStamp;
    }

    public void setOtpTimeStamp(String otpTimeStamp) {
        this.otpTimeStamp = otpTimeStamp;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Boolean getIs2FAEnabled() {
        return is2FAEnabled;
    }

    public void setIs2FAEnabled(Boolean is2FAEnabled) {
        this.is2FAEnabled = is2FAEnabled;
    }

    public void hideSensitiveData(){
        this.setUsername(null);
        this.setPassword(null);
        this.setRegisteredOn(null);
        if(this.getSecretKey()!=null & this.getSecretKey().length()>0)
            this.setIs2FAEnabled(true);
        else
            this.setIs2FAEnabled(false);
        this.setSecretKey(null);
        this.setOtp(null);
        this.setOtpTimeStamp(null);
    }
}
