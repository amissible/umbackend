package com.amissible.um.rest;

import java.util.Optional;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amissible.um.security.model.User;
import com.amissible.um.security.repository.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/unsecured")
public class ResetCredentialsController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	// working
	@GetMapping("/user/recover/username/{email}")
	public ResponseEntity<User> recoverUsername(@PathVariable String email) {
		 return Optional.ofNullable(userRepository.findOneWithAuthoritiesByEmailIgnoreCase(email).get())
			        .map(ResponseEntity::ok)						//200 OK
			        .orElse(ResponseEntity.notFound().build());		//404 Not found
	}
	
	
	@PostMapping("/user/enable")
	public ResponseEntity<User> enableUser(@RequestParam String email) {
		 return Optional.ofNullable(userRepository.findOneWithAuthoritiesByEmailIgnoreCase(email).get())
			        .map(ResponseEntity::ok)						//200 OK
			        .orElse(ResponseEntity.notFound().build());		//404 Not found
	}
	
	@PostMapping("/user/disable")
	public ResponseEntity<User> disableUser(@RequestParam String email) {
		 return Optional.ofNullable(userRepository.findOneWithAuthoritiesByEmailIgnoreCase(email).get())
			        .map(ResponseEntity::ok)						//200 OK
			        .orElse(ResponseEntity.notFound().build());		//404 Not found
	}
	
	@PostMapping("/user/reset/password")
	public ResponseEntity<User> resetPassword(@RequestParam String password, @RequestParam String username) {
		if(Strings.isEmpty(password)) {
			return ResponseEntity.badRequest().build();
		}
		else {
			String encoded = passwordEncoder.encode(password);
			System.out.println(encoded);
			Optional<User> userInSystem = userRepository.findOneWithAuthoritiesByUsername(username);
			if(userInSystem.isPresent()) {
				int isUpdated = userRepository.updateUserPasswordInfoByUsername(encoded, username);
				if(isUpdated == 1) {
					return ResponseEntity.ok().build();	
				}
				else {
					return ResponseEntity.notFound().build();
				}
			}
			else {
				return ResponseEntity.badRequest().build();	
			}
		}
	}
	
	
}
