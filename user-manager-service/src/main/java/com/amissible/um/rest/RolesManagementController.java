package com.amissible.um.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amissible.um.security.model.Authority;
import com.amissible.um.security.repository.AuthorityRepository;

@RestController
@RequestMapping("/unsecured")
public class RolesManagementController {

	@Autowired
	private AuthorityRepository authoritiesRepo;
	
	// working	
	@GetMapping("/roles/all")
	public ResponseEntity<List<Authority>> getAll() {
		return Optional.ofNullable(authoritiesRepo.findAll())
		        .map(ResponseEntity::ok)						//200 OK
		        .orElse(ResponseEntity.notFound().build());		//404 Not found
	}
}
