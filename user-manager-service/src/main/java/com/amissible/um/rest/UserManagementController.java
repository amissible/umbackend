package com.amissible.um.rest;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.amissible.um.security.model.RegisterUser;
import com.amissible.um.security.model.User;
import com.amissible.um.security.repository.UserRepository;

@RestController
@RequestMapping("/unsecured")
public class UserManagementController {

	private AtomicLong counter = new AtomicLong(4);
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	// working
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> getForId(@PathVariable Long id) {
	   return Optional.ofNullable(userRepository.findById(id).get())
	        .map(ResponseEntity::ok)						//200 OK
	        .orElse(ResponseEntity.notFound().build());		//404 Not found
	}
	// working	
	@GetMapping("/user/all")
	public ResponseEntity<List<User>> getAll() {
		return Optional.ofNullable(userRepository.findAll())
		        .map(ResponseEntity::ok)						//200 OK
		        .orElse(ResponseEntity.notFound().build());		//404 Not found
	}
	
	@PostMapping("/user/update")
	public ResponseEntity<User> update(@RequestBody RegisterUser registerUser) {
		Optional<User> existingUser = userRepository.findById(registerUser.getId());
		if(existingUser.isPresent()) {
			String encoded = passwordEncoder.encode(registerUser.getPassword());
			int userUpdated = userRepository.setUserInfoById(registerUser.getFirstname(), registerUser.getLastname(), registerUser.getEmail(), encoded, registerUser.getUsername(), existingUser.get().getId());
			if(userUpdated == 1) {
				return ResponseEntity.ok().build();
			}
			else {
				return ResponseEntity.badRequest().build();
			}
			
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}


	@GetMapping("/user/encoder")
	public String encoder(@RequestParam String x) {
		return passwordEncoder.encode(x);
	}

	// working
	@PostMapping("/user/create")
	public ResponseEntity<User> create(@RequestBody RegisterUser registerUser) {
		String encoded = passwordEncoder.encode(registerUser.getPassword());
		System.out.println(encoded);
		User user = new User();
		user.setId(counter.getAndIncrement());
		user.setActivated(true);
		user.setAuthorities(registerUser.getAuthorities());
		user.setEmail(registerUser.getEmail());
		user.setUsername(registerUser.getUsername());
		user.setFirstname(registerUser.getFirstname());
		user.setLastname(registerUser.getLastname());
		user.setPassword(encoded);
		User userUpdated = userRepository.save(user);
		return ResponseEntity.ok(userUpdated);
	}
	// working
	@GetMapping("/user/delete/{userId}")
	public ResponseEntity<?> delete(@PathVariable long userId) {
		userRepository.deleteById(userId);
		return ResponseEntity.ok("User has been removed");
	}
	// working
	@GetMapping("/user/delete/all")
	public ResponseEntity<?> deleteAll() {
		userRepository.deleteAll();
		return ResponseEntity.ok("User(s) has been removed");
	}

}
