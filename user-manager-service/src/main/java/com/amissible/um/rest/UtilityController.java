package com.amissible.um.rest;

import com.amissible.um.model.Mail;
import com.amissible.um.service.EmailService;
import com.amissible.um.util.TOTPManager;
import com.amissible.um.util.Utility;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/utilServices")
public class UtilityController {

    @JsonProperty("key")
    private String key;

    @Autowired
    private EmailService emailService; //Free Marker


    @Value("${exportDir}")
    String exportDir;

    //EMAIL

    @RequestMapping("/FMsendOTP")
    public void FMsendOTP(@RequestParam String email) throws Exception {
        Mail mail = new Mail();
        mail.setFrom("ps4951426@gmail.com");
        mail.setTo(email);
        mail.setSubject("One Time Password (OTP)");
        mail.setTemplate("sendOTP.ftl");

        String otp = Utility.generateNumericOTP(6);
        // Store event in Database (to,otp,timestamp) for OTP validation

        Map model = new HashMap();
        model.put("otp", otp);
        mail.setModel(model);

        emailService.sendSimpleMessage(mail);
    }

    @RequestMapping("/validateOTP")
    public boolean validateOTP(@RequestParam String email,@RequestParam String otp) throws Exception {
        String DBotp = "";
        String DBtime = "";
        //Fetch DBotp and DBtime where email is 'email'
        if(DBotp.equals(otp))
            return true;
        else
            return false;
    }

    @RequestMapping("/freeMarker")
    public void freemarker(@RequestParam String email, @RequestParam String subject) throws Exception {
        Mail mail = new Mail();
        mail.setFrom("ps4951426@gmail.com");
        mail.setTo(email);
        mail.setSubject(subject);
        mail.setTemplate("email-template.ftl");

        Map model = new HashMap();
        model.put("name", "Omendra");
        model.put("location", "Gorakhpur");
        model.put("signature", "Amissible");
        mail.setModel(model);

        emailService.sendSimpleMessage(mail);
    }

    @RequestMapping("/freeMarkerAttached")
    public void freemarker(@RequestParam String to, @RequestParam String subject, @RequestParam String template,@RequestParam String[] files) throws Exception {
        Mail mail = new Mail();
        mail.setFrom("ps4951426@gmail.com");
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setTemplate(template);
        mail.setAttachments(files);

        Map model = new HashMap();
        model.put("name", "Omendra");
        model.put("location", "Gorakhpur");
        model.put("signature", "Amissible");
        mail.setModel(model);

        emailService.sendSimpleMessage(mail);
    }

    @RequestMapping("/freeMarkerEmail")
    public void freemarkerEmail(@RequestParam String to, @RequestParam String subject,
                                @RequestParam String template, @RequestParam Map<String,String> model) throws Exception {
        Mail mail = new Mail();
        mail.setFrom("ps4951426@gmail.com");
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setTemplate(template);
        mail.setModel(model);

        emailService.sendSimpleMessage(mail);
    }

    @RequestMapping("/freeMarkerAttachedEmail")
    public void freemarkerEmail(@RequestParam String to, @RequestParam String subject, @RequestParam String template,
                                @RequestParam Map<String,String> model,@RequestParam String[] files) throws Exception {
        Mail mail = new Mail();
        mail.setFrom("ps4951426@gmail.com");
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setTemplate(template);
        mail.setModel(model);
        mail.setAttachments(files);
        emailService.sendSimpleMessage(mail);
    }

/*
    @RequestMapping("/updateTemplate/emailBody")
    public void updateEmailBody(@RequestParam String emailType, @RequestParam String emailBody){
        emailTemplateDAO.updateTemplateBody(emailType,emailBody);
    }*/
/*
    @RequestMapping("/deleteTemplate")
    public void saveTemplate(@RequestParam String emailType){
        emailTemplateDAO.deleteTemplate(emailType);
    }
*/
    //TWO FACTOR AUTHENTICATION

    @RequestMapping(value = "/2FA/newSecretKey")
    public ResponseEntity newSecretKey(){
        key = TOTPManager.generateSecretKey();
        return new ResponseEntity(key, HttpStatus.OK);
    }

    @RequestMapping(value = "/2FA/getQR")
    public @ResponseBody
    ResponseEntity getQR(@RequestParam String secretKey, @RequestParam String email, @RequestParam String issuer){
        try{
            String path = exportDir+"/"+secretKey+".png";
            String barCodeUrl = TOTPManager.getGoogleAuthenticatorBarCode(secretKey, email, issuer);
            TOTPManager.createQRCode(barCodeUrl,path, 400, 400);
            Path filePath = Paths.get(path);
            if (Files.exists(filePath)) {
                /*emailSender.sendEmail(email,null,null,"QR Authentication",
                        "Please scan the below QR code in Authenticator App",
                        new String[]{path});*/
                return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(Files.readAllBytes(filePath));
            }
            else
                throw new RuntimeException("File Not Found");
        }
        catch (Exception e){
            throw new RuntimeException("ERROR = "+e.getMessage());
        }
    }

    @RequestMapping(value = "/2FA/verifyTOTP")
    public @ResponseBody
    Boolean verifyTOTP(@RequestParam String secretKey,@RequestParam String code) {
        String otp = TOTPManager.getTOTPCode(secretKey);
        if (code.equals(otp)) {
            return true;
        } else {
            return false;
        }
    }
}
