package com.amissible.um.security.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.amissible.um.security.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

   @EntityGraph(attributePaths = "authorities")
   Optional<User> findOneWithAuthoritiesByUsername(String username);

   @EntityGraph(attributePaths = "authorities")
   Optional<User> findOneWithAuthoritiesByEmailIgnoreCase(String email);

   @Transactional
   @Modifying
   @Query("update User u set u.firstname = ?1, u.lastname = ?2, u.email = ?3, u.password = ?4, u.username = ?5  where u.id = ?6")
   int setUserInfoById(String firstname, String lastname, String email, String password, String username,  Long userId);
   
   
   @Transactional
   @Modifying
   @Query("update User u set u.password = ?1 where u.username = ?2")
   int updateUserPasswordInfoByUsername(String password, String username);
}
