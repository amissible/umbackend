package com.amissible.um.service;

import com.amissible.um.exception.AppException;
import com.amissible.um.model.Mail;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Primary
@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private Configuration freemarkerConfig;

    @Value("${spring.mail.username}")
    String from;

    public void sendSimpleMessage(Mail mail){
        try{
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            String attachments[] = mail.getAttachments();
            if(attachments != null){
                for(String path : attachments){
                    File file = new File(path);
                    helper.addAttachment(file.getName(),file);
                }
            }
            //helper.addAttachment("logo.jpg", new ClassPathResource("/logo.jpg"));
            //helper.addAttachment("logo.jpg",new File("/logo.jpg"));

            Template t = freemarkerConfig.getTemplate(mail.getTemplate());
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());

            helper.setTo(mail.getTo());
            helper.setText(html, true);
            helper.setSubject(mail.getSubject());
            helper.setFrom(from);

            emailSender.send(message);
        }
        catch (Exception e){
            throw new AppException("EMAIL_SERVICE_ERROR : "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
