package com.amissible.um.service;

import com.amissible.um.dao.AuthorityDAO;
import com.amissible.um.dao.UserAuthorityDAO;
import com.amissible.um.dao.UserDAO;
import com.amissible.um.exception.AppException;
import com.amissible.um.model.Authority;
import com.amissible.um.model.Mail;
import com.amissible.um.model.Status;
import com.amissible.um.model.User;
import com.amissible.um.util.TOTPManager;
import com.amissible.um.util.Utility;
import com.google.zxing.WriterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class UMService {

    @Autowired
    UserDAO userDAO;

    @Autowired
    AuthorityDAO authorityDAO;

    @Autowired
    UserAuthorityDAO userAuthorityDAO;

    @Autowired
    private EmailService emailService;

    @Value("${organization}")
    String org;

    @Value("${exportDir}")
    String exportDir;

    public User verifyLoginData(String username,String password){
        User user = userDAO.getUserByUsername(username);
        if(user == null)
            user = userDAO.getUserByEmail(username);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        if(!user.getPassword().equals(password))
            throw new AppException("INVALID_CREDENTIALS",HttpStatus.BAD_REQUEST);
        if(!user.getStatus().equals(Status.ACTIVE.toString()) && !user.getStatus().equals(Status.UNVERIFIED.toString()))
            throw new AppException("USER_STATUS_IS_"+user.getStatus(),HttpStatus.UNAUTHORIZED);
        user.hideSensitiveData();
        return user;
    }

    public ResponseEntity enable2FA(String email){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        String secretKey;
        do{
            secretKey = TOTPManager.generateSecretKey();
        }while(userDAO.secretKeyPresent(secretKey));
        userDAO.updateSecretKey(email,secretKey);
        String path = exportDir+"/"+secretKey+".png";
        String barCodeUrl = TOTPManager.getGoogleAuthenticatorBarCode(secretKey, email, org);
        try{
            TOTPManager.createQRCode(barCodeUrl,path, 400, 400);
        }
        catch (Exception e){
            throw new AppException("QR_CODE_GENERATION_FAILED",HttpStatus.EXPECTATION_FAILED);
        }
        Path filePath = Paths.get(path);
        if (Files.exists(filePath)) {
            Mail mail = new Mail();
            mail.setTemplate("sendTotpQrCode.ftl");
            mail.setTo(email);
            mail.setSubject("QR Authentication");
            mail.setAttachments(new String[]{path});
            emailService.sendSimpleMessage(mail);
            try{
                return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(Files.readAllBytes(filePath));
            }
            catch (Exception e){
                throw new AppException("READING_QR_CODE_FAILED",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else
            throw new AppException("QR_CODE_NOT_FOUND",HttpStatus.NOT_FOUND);
    }

    public void disable2FA(String email){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        userDAO.updateSecretKey(email,"");
    }

    public Boolean verifyTOTP(String email,String code) {
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        if(user.getSecretKey() == null || user.getSecretKey().length() == 0)
            throw new AppException("TWO_FACTOR_AUTHENTICATION_NOT_ENABLED",HttpStatus.METHOD_NOT_ALLOWED);
        String otp = TOTPManager.getTOTPCode(user.getSecretKey());
        if (code.equals(otp)) {
            return true;
        } else {
            return false;
        }
    }

    public void sendOTP(String email){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        Mail mail = new Mail();
        mail.setTo(email);
        mail.setSubject("One Time Password (OTP)");
        mail.setTemplate("sendOTP.ftl");
        String otp = Utility.generateNumericOTP(6);
        Map model = new HashMap();
        model.put("otp", otp);
        mail.setModel(model);
        emailService.sendSimpleMessage(mail);
        userDAO.updateOTP(email,otp,Utility.nowDateTime());
    }

    public Boolean verifyOTP(String email,String otp){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        if(user.getOtp().equals(otp)){
            Long diff = Timestamp.valueOf(Utility.nowDateTime()).getTime() - Timestamp.valueOf(user.getOtpTimeStamp()).getTime();
            diff = diff / (60 * 1000);
            if(diff < 5)
                return true;
        }
        return false;
    }

    public void recoverCredentials(String email,String credentialType){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        Mail mail = new Mail();
        mail.setTo(email);
        mail.setSubject("CREDENTIALS RECOVERY");
        mail.setTemplate("recoverCredentials.ftl");
        Map model = new HashMap();
        model.put("credentialType", credentialType);
        if(credentialType.equals("Username"))
            model.put("credentialValue", user.getUsername());
        else if(credentialType.equals("Password"))
            model.put("credentialValue", user.getPassword());
        mail.setModel(model);
        emailService.sendSimpleMessage(mail);
    }

    public void addAuthority(String name){
        authorityDAO.addAuthority(new Authority(name));
    }

    public void removeAuthority(String name){
        authorityDAO.removeAuthority(new Authority(name));
    }

    public List<Authority> getAllAuthorities(){
        return authorityDAO.getAllAuthorities();
    }

    public void assignAuthority(String email,String authorityName){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        userAuthorityDAO.assignAuthority(user.getId(),new Authority(authorityName));
    }

    public void revokeAllAuthoritiesFromUser(String email){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        userAuthorityDAO.revokeAllAuthoritiesFromUser(user.getId());
    }

    public void revokeAuthorityFromUser(String email,String authorityName){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        userAuthorityDAO.revokeAuthorityFromUser(user.getId(),new Authority(authorityName));
    }

    public Set<Authority> getUserAuthorities(String email){
        User user = userDAO.getUserByEmail(email);
        if(user == null)
            throw new AppException("USER_NOT_REGISTERED", HttpStatus.NOT_FOUND);
        return userAuthorityDAO.getUserAuthorities(user.getId());
    }

    public void registerUser(String firstName,String lastName,String email,String username,String password,String roles[]){
        Long id = userDAO.registerUser(firstName,lastName,email,username,password);
        if(roles != null){
            for(String role : roles)
                userAuthorityDAO.assignAuthority(id,new Authority(role));
        }
    }
}
