package com.amissible.um.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utility {

    public static String generateNumericOTP(Integer length){
        String OTP = "";
        for(int i=1;i<=length;i++)
            OTP = OTP + (int)(Math.random()*10);
        return OTP;
    }

    public static String generateAlphaNumericOTP(Integer length){
        String OTP = "";
        for(int i=1;i<=length;i++){
            if(i%2 == 0)
                OTP = OTP + (char)(65+Math.random()*26);
            else
                OTP = OTP + (int)(Math.random()*10);
        }
        return OTP;
    }

    public static String nowDateTime(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(Calendar.getInstance().getTime());
    }
}
